import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit
import torchvision.transforms as transforms
import cv2
import time
import numpy as np
from PIL import Image
import glob
import torch

TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
def load_trt(filepath):
    with open(filepath, "rb") as file, trt.Runtime(TRT_LOGGER) as runtime:
        engine = runtime.deserialize_cuda_engine(file.read())
    return engine

transform = transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize((64, 64)),
        transforms.ToTensor(),])

def preprocess(image):
    # Mean normalization
    # mean = np.array([0.485, 0.456, 0.406]).astype('float32')
    # stddev = np.array([0.229, 0.224, 0.225]).astype('float32')
    # data = (np.asarray(image).astype('float32') / float(255.0) - mean) / stddev
    # data = np.expand_dims(data, axis=0)

    # convert the image from BGR to RGB color format
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    # apply image transforms
    image = transform(image)
    data = np.expand_dims(np.array(image), axis=0)
    # Switch from HWC to to CHW order
    # return np.moveaxis(data, 3, 1)
    # image = torch.unsqueeze(image, 0)
    return data

def postprocess(data):
    num_classes = 21
    # create a color palette, selecting a color for each class
    palette = np.array([2 ** 25 - 1, 2 ** 15 - 1, 2 ** 21 - 1])
    colors = np.array([palette*i%255 for i in range(num_classes)]).astype("uint8")
    # plot the segmentation predictions for 21 classes in different colors
    img = Image.fromarray(data.astype('uint8'), mode='P')
    img.putpalette(colors)
    return img

threshold = 0.5
data_path = ""

def infer(engine, img):
    # img = cv2.imread(input_file)
    # with cv2.imread(input_file) as img:
    input_image = preprocess(img)
    # print(input_image.shape)
    image_width = input_image.shape[2]
    image_height = input_image.shape[3]

    with engine.create_execution_context() as context:
        # Set input shape based on image dimensions for inference
        context.set_binding_shape(engine.get_binding_index("input"), (1, 3, image_height, image_width))
        # Allocate host and device buffers
        bindings = []
        for binding in engine:
            # print(binding)
            binding_idx = engine.get_binding_index(binding)
            size = trt.volume(context.get_binding_shape(binding_idx))
            # print(size)
            dtype = trt.nptype(engine.get_binding_dtype(binding))
            if engine.binding_is_input(binding):
                input_buffer = np.ascontiguousarray(input_image)
                input_memory = cuda.mem_alloc(input_image.nbytes)
                bindings.append(int(input_memory))
            else:
                output_buffer = cuda.pagelocked_empty(size, dtype)
                output_memory = cuda.mem_alloc(output_buffer.nbytes)
                bindings.append(int(output_memory))

        stream = cuda.Stream()
        # Transfer input data to the GPU.
        # print(input_buffer.shape, "input_buffer")
        start = time.time()
        cuda.memcpy_htod_async(input_memory, input_buffer, stream)
        # Run inference
        context.execute_async_v2(bindings=bindings, stream_handle=stream.handle)
        # Transfer prediction output from the GPU.
        cuda.memcpy_dtoh_async(output_buffer, output_memory, stream)
        stop = time.time()
        # Synchronize the stream
        stream.synchronize()
        print("time", stop - start)
        # print(output_buffer)
        outputs = torch.sigmoid(torch.from_numpy(output_buffer))
        # print(outputs, input_file)
        outputs = outputs.detach().cpu()
        string_predicted = ""
        if outputs[0] > threshold:
            string_predicted += "vest_"
        else: 
            string_predicted += "novest_"

        if outputs[1] > threshold:
            string_predicted += "helmet_"
        else: 
            string_predicted += "nohelmet_"
        return string_predicted[:-1]