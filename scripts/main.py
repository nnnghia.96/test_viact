#!/usr/bin/env python3
import cv2
import os
import shutil
import sys
import time
sys.path.append("./")

from config import config
from core.cameras import camera_factory
from core.display import Display
from core.display.utils import draw_detections
from core.display.utils import draw_tracked_objects
from core.display.utils import draw_zones
from core.logging import logger
from core.models import architectures
from core.monitor import Monitor
from core.services import alarm
from core.trackers import DetectedObject
from core.utils.bbox import expand_bbox
from core.utils.bbox import filter_bbox
from core.utils.engine import load_engine
from core.workers import alarm_queue
from core.workers import queue
from core.workers import video_queue
from pprint import pprint
# from detect import detect
from model import DetectMultiBackend
from utils import check_img_size, non_max_suppression, xyxy2xywh, letterbox, Annotator, colors, save_one_box, scale_coords
import cv2
import torch
from pathlib import Path
import numpy as np
from inference import load_trt, infer

device = 'cuda'
data = "coco.yaml"
dnn = True
half = True
weights = "yolov5l6.engine"
img_path = "bus.jpg"
video_path = "demo_test.mp4"
save_dir = "test"
conf_thres=0.25  # confidence threshold
iou_thres=0.45  # NMS IOU threshold
max_det=1000  # maximum detections per image
classes = 0 # person
agnostic_nms = False
save_crop = True
line_thickness = 3
save_txt = False
save_img = True
view_img = True
hide_labels = False
hide_conf = False
p = "xxx.jpg"


trt_path = "repvggA0_engine.trt"
engine = load_trt(trt_path)

model = DetectMultiBackend(weights, device=device, dnn=dnn, data=data, fp16=half)
stride, names = model.stride, model.names
imgsz = (1280, 1280)

cap = cv2.VideoCapture(video_path)
frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))   
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
frameTime = 1
fps = 30 / frameTime

size = (frame_width, frame_height)

def detect(frame):
    bboxes = []
    confes = []
    clses = []
    descriptions = []
    imgsz = check_img_size(imgsz, s=stride)  # check image size

    # im0 = cv2.imread(img_path)
    im0 = frame
    im = letterbox(im0, imgsz, stride=stride, auto=False)[0]  # padded resize
    im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
    im = np.ascontiguousarray(im)  # contiguous


    im = torch.from_numpy(im).to(device)
    im = im.half() if model.fp16 else im.float()  # uint8 to fp16/32
    im /= 255  # 0 - 255 to 0.0 - 1.0
    if len(im.shape) == 3:
        im = im[None]  # expand for batch dim

    pred = model(im, augment=True)
    pred = non_max_suppression(pred, conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)

    seen = 0
    # cv2.imshow('Frame',frame)
    # for i, det in enumerate(pred):  # per image
    det = pred[0]
    # seen += 1
    gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
    imc = im0.copy() if save_crop else im0  # for save_crop
    annotator = Annotator(im0, line_width=line_thickness, example=str(names))
    if len(det):
        # Rescale boxes from img_size to im0 size
        det[:, :4] = scale_coords(im.shape[2:], det[:, :4], im0.shape).round()

        # Print results
        for c in det[:, -1].unique():
            n = (det[:, -1] == c).sum()  # detections per class

        # Write results
        for *xyxy, conf, cls in reversed(det):
            if save_txt:  # Write to file
                xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
            if save_img or save_crop or view_img:  # Add bbox to image
                c = int(cls)  # integer class
                label = None if hide_labels else (names[c] if hide_conf else f'{names[c]} {conf:.2f}')
                if "person" in label:
                    crop = save_one_box(xyxy, imc, file="xxx.jpg", BGR=True)
                    lb = infer(engine, crop)
                    # annotator.box_label(xyxy, lb, color=colors(c, True))
                    bboxes.append(xyxy)
                    confes.append(conf)
                    clses.append(cls)
                    descriptions.append(lb)
    # im0 = annotator.result()
        return bboxes, confes, clses, descriptions

def main():

    if os.path.exists("tmp"): shutil.rmtree("tmp")
    os.makedirs("tmp", exist_ok=True)
    os.makedirs("persist", exist_ok=True)

    logger.info(f"Machine\t: {config.machine}")
    logger.info(f"Mode\t\t: {config.mode}")

    logger.info("Loading task queues ...")
    queue.start()
    video_queue.start()
    alarm_queue.start()

    logger.info("Loading cameras ...")
    # print(config.cameras)
    cameras = { c.name: camera_factory[c.type](c.url, c.name) for c in config.cameras }

    logger.info(f"Loading models ...")
    # models = {}
    # for model in config.models:
    #     # Load model engine
    #     engine = load_engine(model.trt_path)
    #     # Load classes
    #     classes_file = open(model.classes_path, "r")
    #     classes = [line.strip() for line in classes_file.readlines()]
    #     # Create model
    #     input_dim = (model.input_w, model.input_h)
    #     models[model.name] = architectures[model.architecture](engine, input_dim, classes)
    #     logger.info(f"Loaded model {model.name}.")

    logger.info("Loading Monitors ...")
    monitors = []
    for camera_name, monitor_configs in config.monitors.items():
        for direction, monitor_config in enumerate(monitor_configs):
            name = f"{config.machine}-{camera_name}"
            if len(monitor_configs) > 1: name += f"({direction})"
            monitors.append(Monitor(name, cameras[camera_name], monitor_config))
            logger.info(f"Loaded monitor {name}.")

    logger.info("Loading Display ...")
    display = Display(len(monitors))

    while True:
        try:

            # Update camera / monitor frames
            [camera.read() for camera in cameras.values()]
            frames = [monitor.read() for monitor in monitors]

            # all_detections: monitor name -> [DetectedObject]
            all_detections = {}
            for i, (monitor, frame) in enumerate(zip(monitors, frames)):

                # Skip detection if frame is None
                if frame is None:
                    all_detections[monitor.name] = []
                    continue

                # Detect objects within frame
                detected_objects = []
                #bboxs, scores, labels = models["head"].infer(frame)
                # bboxs, labels = models["head"].infer(frame)
                # #bboxs, labels = models["person"].infer(frame)
                # for (x1, y1, x2, y2, score), label in zip(bboxs, labels):
                    
                #     # Skip bbox with unreasonable dimensions
                #     # if not filter_bbox(frame, (x1, y1, x2, y2)): continue

                #     # Crop bbox region for second stage detection
                #     bbox = tuple(map(int, [x1, y1, x2, y2]))
                #     detected_object = DetectedObject(
                #         bbox,
                #         confidence=score,
                #         type=label
                #     )
                #     region = expand_bbox(frame, bbox, ratio=0.01)

                #     # Additional labels
                #     helmet_label = models["helmet"].infer(region)
                #     mask_label = models["mask"].infer(region)
                #     # if "undetermined" not in [helmet_label, jacket_label]:

                #     detected_object.descriptions = {"helmet": helmet_label, "mask": mask_label}
                #     #detected_object.descriptions = {"mask": mask_label }
                #     #detected_object.descriptions = {"helmet": helmet_label }
                #     detected_objects.append(detected_object)
                    

                #-----------------------------------------------------------------------------------
                bboxes, confes, clses, descriptions = detect(frame)
                # for (x1, y1, x2, y2), score, label in zip(bboxs, scores, labels):
                for xyxy, conf, cls, desc in zip(bboxes, confes, clses, descriptions):
                    x1, y1, x2, y2 = xyxy
                    bbox = tuple(map(int, [x1, y1, x2, y2]))
                    detected_object = DetectedObject(
                        bbox,
                        confidence=conf,
                        type=cls
                    )
                    detected_object.descriptions = {"label": desc}
                    detected_objects.append(detected_object)
                    # label = None if hide_labels else (names[c] if hide_conf else f'{names[c]} {conf:.2f}')
                    # if "person" in label:
                    #     crop = save_one_box(xyxy, imc, file="xxx.jpg", BGR=True)
                # Handle detections
                detected_objects = monitor.handle_detections(frame, detected_objects)
                all_detections[monitor.name] = detected_objects

            # Visualize detections
            # frames = [draw_detections(frame, detections) for frame, detections in zip(frames, all_detections.values())]

            # Visualize centroid tracker
            if config.display.display_centroids:
                tracked_objects_list = [monitor.tracked_objects for monitor in monitors]
                frames = [draw_tracked_objects(frame, tracked_objects) for frame, tracked_objects in zip(frames, tracked_objects_list)]

            # Visualize zones
            if config.display.display_zones:
                frames = [draw_zones(frame, monitor.zones) for frame, monitor in zip(frames, monitors)]

            display.show(frames)
            key = cv2.waitKey(1)
            if key == ord("q"): break
            elif key > -1 and chr(key) in [str(i) for i in range(len(monitors))]:
                monitor = monitors[int(chr(key))]
                monitor.send_alert(monitor.engines)
                logger.info(f"Sending alert for monitor {monitor.name} ...")
                if monitor.trigger_alarm:
                    alarm_queue.enqueue(alarm.trigger_alarm, args=(monitor.camera_name, ), name=f"monitor {monitor.name} alarm")

        except KeyboardInterrupt:
            break

    logger.info("Cleaning up ...")
    [camera.stop() for camera in cameras.values()]
    [monitor.stop() for monitor in monitors]
    # Close Queues
    queue.close()
    video_queue.close()
    alarm_queue.close()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
