from model import DetectMultiBackend
from utils import check_img_size, non_max_suppression, xyxy2xywh, letterbox, Annotator, colors, save_one_box, scale_coords
import cv2
import torch
from pathlib import Path
import numpy as np
from inference import load_trt, infer

device = 'cuda'
data = "coco.yaml"
dnn = True
half = True
weights = "yolov5l6.engine"
img_path = "bus.jpg"
video_path = "demo_test.mp4"
save_dir = "test"
conf_thres=0.25  # confidence threshold
iou_thres=0.45  # NMS IOU threshold
max_det=1000  # maximum detections per image
classes = 0 # person
agnostic_nms = False
save_crop = True
line_thickness = 3
save_txt = False
save_img = True
view_img = True
hide_labels = False
hide_conf = False
p = "xxx.jpg"


trt_path = "repvggA0_engine.trt"
engine = load_trt(trt_path)

model = DetectMultiBackend(weights, device=device, dnn=dnn, data=data, fp16=half)
stride, names = model.stride, model.names
imgsz = (1280, 1280)

cap = cv2.VideoCapture(video_path)
frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))   
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
frameTime = 1
fps = 30 / frameTime

size = (frame_width, frame_height)
# result = cv2.VideoWriter('filename.mp4', 
#                          cv2.VideoWriter_fourcc(*'MJPG'),
#                          fps, size)
# while(cap.isOpened()):
#     ret, frame = cap.read()
#     if ret == True:
#         imgsz = check_img_size(imgsz, s=stride)  # check image size

#         # im0 = cv2.imread(img_path)
#         im0 = frame
#         im = letterbox(im0, imgsz, stride=stride, auto=False)[0]  # padded resize
#         im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
#         im = np.ascontiguousarray(im)  # contiguous


#         im = torch.from_numpy(im).to(device)
#         im = im.half() if model.fp16 else im.float()  # uint8 to fp16/32
#         im /= 255  # 0 - 255 to 0.0 - 1.0
#         if len(im.shape) == 3:
#             im = im[None]  # expand for batch dim

#         pred = model(im, augment=True)
#         pred = non_max_suppression(pred, conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)

#         seen = 0
#         # cv2.imshow('Frame',frame)
#         for i, det in enumerate(pred):  # per image
#             seen += 1
#             gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
#             imc = im0.copy() if save_crop else im0  # for save_crop
#             annotator = Annotator(im0, line_width=line_thickness, example=str(names))
#             if len(det):
#                 # Rescale boxes from img_size to im0 size
#                 det[:, :4] = scale_coords(im.shape[2:], det[:, :4], im0.shape).round()

#                 # Print results
#                 for c in det[:, -1].unique():
#                     n = (det[:, -1] == c).sum()  # detections per class

#                 # Write results
#                 for *xyxy, conf, cls in reversed(det):
#                     if save_txt:  # Write to file
#                         xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
#                     if save_img or save_crop or view_img:  # Add bbox to image
#                         c = int(cls)  # integer class
#                         label = None if hide_labels else (names[c] if hide_conf else f'{names[c]} {conf:.2f}')
#                         if "person" in label:
#                             crop = save_one_box(xyxy, imc, file="xxx.jpg", BGR=True)
#                             lb = infer(engine, crop)
#                             annotator.box_label(xyxy, lb, color=colors(c, True))
#             im0 = annotator.result()
#             # cv2.imwrite(str(i) + ".jpg", im0)
                
#             result.write(im0)
#         if cv2.waitKey(frameTime) & 0xFF == ord('q'):
#             break
#     else:
#         break

# cap.release()
# result.release()
# cv2.destroyAllWindows()

def detect(engine, frame):
    bboxes = []
    confes = []
    clses = []
    descriptions = []
    imgsz = check_img_size(imgsz, s=stride)  # check image size

    # im0 = cv2.imread(img_path)
    im0 = frame
    im = letterbox(im0, imgsz, stride=stride, auto=False)[0]  # padded resize
    im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
    im = np.ascontiguousarray(im)  # contiguous


    im = torch.from_numpy(im).to(device)
    im = im.half() if model.fp16 else im.float()  # uint8 to fp16/32
    im /= 255  # 0 - 255 to 0.0 - 1.0
    if len(im.shape) == 3:
        im = im[None]  # expand for batch dim

    pred = model(im, augment=True)
    pred = non_max_suppression(pred, conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)

    seen = 0
    # cv2.imshow('Frame',frame)
    # for i, det in enumerate(pred):  # per image
    det = pred[0]
    # seen += 1
    gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
    imc = im0.copy() if save_crop else im0  # for save_crop
    annotator = Annotator(im0, line_width=line_thickness, example=str(names))
    if len(det):
        # Rescale boxes from img_size to im0 size
        det[:, :4] = scale_coords(im.shape[2:], det[:, :4], im0.shape).round()

        # Print results
        for c in det[:, -1].unique():
            n = (det[:, -1] == c).sum()  # detections per class

        # Write results
        for *xyxy, conf, cls in reversed(det):
            if save_txt:  # Write to file
                xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
            if save_img or save_crop or view_img:  # Add bbox to image
                c = int(cls)  # integer class
                label = None if hide_labels else (names[c] if hide_conf else f'{names[c]} {conf:.2f}')
                if "person" in label:
                    crop = save_one_box(xyxy, imc, file="xxx.jpg", BGR=True)
                    lb = infer(engine, crop)
                    # annotator.box_label(xyxy, lb, color=colors(c, True))
                    bboxes.append(xyxy)
                    confes.append(conf)
                    clses.append(cls)
                    descriptions.append(lb)
    # im0 = annotator.result()
        return bboxes, confes, clses, descriptions