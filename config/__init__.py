import yaml
from easydict import EasyDict

with open("./config/config.yml", "r") as file:
    config = EasyDict(yaml.safe_load(file))

with open("./config/rules.yml", "r") as file:
    engine_rules = EasyDict(yaml.safe_load(file))
