import json
import requests
from .network import check_internet_connection
from config import config
from pprint import pprint

headers = { "x-customindz-key" : "customindz", "Content-type": "application/json" }

def put_thumbnail(monitor_id, image_url):
    check_internet_connection()
    data = { "snapshot": image_url }
    requests.put(f"{config.dashboard_api}/monitor/{monitor_id}", json=data, headers=headers)

def post_alert(monitor_id, event, image_url=None, video_url=None, attributes=None):
    check_internet_connection()
    data = { "alert": "Y", "engine": event, "monitor_id": monitor_id }
    if image_url is not None:
        data["image_url"] = image_url
    if image_url is not None:
        data["video_url"] = video_url
    if attributes is not None:
        data["attributes"] = attributes
    response = requests.post(f"{config.dashboard_api}/detection/incoming", json=data, headers=headers)
    response = json.loads(response.content.decode("utf-8"))
    return response

def update_alert(alert_id, image_url=None, video_url=None):
    check_internet_connection()
    data = {}
    if image_url is not None:
        data["image_url"] = image_url
    if video_url is not None:
        data["video_url"] = video_url
    response = requests.post(f"{config.dashboard_api}/detection/{alert_id}", json=data, headers=headers)
    response = json.loads(response.content.decode("utf-8"))
    return response

def get_monitor_config(monitor_id):
    check_internet_connection()
    response = requests.get(f"{config.dashboard_api}/monitor/{monitor_id}", headers=headers)
    if response.status_code == 200:
        return json.loads(response.content.decode("utf-8"))
