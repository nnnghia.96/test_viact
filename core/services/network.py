import requests

def check_internet_connection():
    try:
        requests.get("https://www.google.com", timeout=3)
    except Exception as exception:
        raise Exception("No internet connection")
