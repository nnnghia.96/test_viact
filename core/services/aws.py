import boto3
import os
from .network import check_internet_connection
from pprint import pprint

session = boto3.Session()
client = session.client("s3")

def download_file(bucket, url, filepath):
    if not os.path.exists(filepath):
        check_internet_connection()
        client.download_file(bucket, url, filepath)

def upload_file(bucket, url, filepath):
    if os.path.exists(filepath):
        check_internet_connection()
        client.upload_file(filepath, bucket, url, ExtraArgs={'ACL': 'public-read'})
        os.remove(filepath)
        return f"https://{bucket}.s3.ap-east-1.amazonaws.com/{url}"

def get_all_objects(bucket, url):
    check_internet_connection()
    objects = client.list_objects_v2(Bucket=bucket, Prefix=f"{url}/")
    while objects["IsTruncated"]:
        for object in objects.get("Contents", []): yield(object)
        objects = client.list_objects_v2(Bucket=bucket, Prefix=f"{url}/", ContinuationToken=objects["NextContinuationToken"])
    for object in objects.get("Contents", []): yield(object)

def download_object(bucket, url, file):
    check_internet_connection()
    client.download_fileobj(bucket, url, file)
