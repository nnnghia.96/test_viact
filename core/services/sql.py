import io
import numpy as np
import sqlite3
import zlib
from config import config

def array_adapter(array):
    output = io.BytesIO()
    np.save(output, array)
    output.seek(0)
    return sqlite3.Binary(zlib.compress(output.read()))

def array_converter(string):
    output = io.BytesIO(string)
    output.seek(0)
    output = io.BytesIO(zlib.decompress(output.read()))
    return np.load(output)

sqlite3.register_adapter(np.ndarray, array_adapter)
sqlite3.register_converter("array", array_converter)

connection = None

def connect():
    global connection
    if connection is None:
        connection = sqlite3.connect(config.sql_host, detect_types=sqlite3.PARSE_DECLTYPES)
    return connection.cursor()

def close():
    global connection
    if connection:
        connection.close()
        connection = None
