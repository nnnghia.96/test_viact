import requests
import time
from config import config

def trigger_alarm(camera_name):
    requests.get(f"{config.alarm_api[camera_name]}/enable")
