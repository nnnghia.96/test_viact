import cv2
import numpy as np
import pytz
from config import engine_rules
from datetime import datetime
from datetime import time

class Zone:

    def __init__(self, type, config):
        self.name = config.name
        self.color = self._hex_to_bgr(config.zoneColor)

        # Zone dimensions
        self.input_h = config.height
        self.input_w = config.width
        self.type = type
        if self.type == "circle":
            self.cx = int(config.circle.x)
            self.cy = int(config.circle.y)
            self.r = int(config.circle.radius)
        if self.type == "square":
            self.x1 = min(int(config.square.x), int(config.square.x + config.square.width))
            self.x2 = max(int(config.square.x), int(config.square.x + config.square.width))
            self.y1 = min(int(config.square.y), int(config.square.y + config.square.height))
            self.y2 = max(int(config.square.y), int(config.square.y + config.square.height))
        if self.type == "polygon":
            coords = config.polygon
            coords = [coords[i] for i in range(0, len(coords), 2)]
            coords = [[coord.x, coord.y] for coord in coords]
            self.coords = np.array(coords).astype("int")

        # Zone engines
        self.engines = config.engines.split("_")

        # Active times
        self.timezone = pytz.timezone("Asia/Hong_Kong")
        self.active_days = self._days_to_ints(config.activeDays)
        self.start_time, self.end_time = self._utc_to_time(config.startTime, config.endTime)

    def validate(self, frame_dim, tracked_objects):
        # Check active day
        today = datetime.today().weekday()
        if today not in self.active_days: return

        # Check active times
        now = datetime.now().astimezone(self.timezone).time()
        if now < self.start_time or now > self.end_time: return

        # Only update tracked objects in the current frame
        tracked_objects = [obj for obj in tracked_objects if obj.detected]

        for engine in self.engines:
            if engine not in engine_rules: continue
            
            # Get the rules for this engine
            engine_rule = engine_rules[engine]
            engine_logic = engine_rule.get("logic", "standard")

            # Standard logic: Trigger alert if "type" with invalid "descriptions" appear in frame
            if engine_logic == "standard":
                for tracked_object in [obj for obj in tracked_objects if obj.type in engine_rule.type]:
            
                #for tracked_object in tracked_objects:
                    # Check if object is inside zone
                    if self._inside_zone(frame_dim, tracked_object):
                        # Engine has no allowed category
                        if not engine_rule.allow:
                            tracked_object.add_trigger_event(engine)
                        else:
                            # Get allowed descriptions for each description key
                            for key, allow_values in engine_rule.allow.items():
                                if tracked_object.descriptions[key] not in allow_values:
                                    tracked_object.add_trigger_event(engine)
                                    break

    def _inside_zone(self, frame_dim, object):
        frame_h, frame_w = frame_dim
        cx, cy = object.centroid
        # Rescale centroid coordinates to zone dimensions
        centroid = (cx / frame_w, cy / frame_h)
        centroid = np.array(centroid) * np.array([self.input_w, self.input_h])
        if self.type == "circle":
            centroid = centroid.astype("int")
            return np.linalg.norm(centroid - np.array([self.cx, self.cy])) < self.r
        if self.type == "square":
            centroid = centroid.astype("int")
            return centroid[0] > self.x1 and centroid[0] < self.x2 \
                    and centroid[1] > self.y1 and centroid[1] < self.y2
        if self.type == "polygon":
            centroid = tuple(centroid.astype("float"))
            return cv2.pointPolygonTest(self.coords, centroid, False) > 0

    def _hex_to_bgr(self, color_hex):
        color_hex = color_hex.lstrip("#")
        return tuple(int(color_hex[i:i+2], 16) for i in [4, 2, 0])

    def _days_to_ints(self, days):
        day_dict = dict((v, k) for k, v in enumerate(["mon", "tue", "wed", "thu", "fri", "sat", "sun"]))
        return [day_dict[day] for day in days]

    def _utc_to_time(self, start_string, end_string):
        try:
            # Parse start time
            start = datetime.strptime(start_string, "%Y-%m-%dT%H:%M:%S.%fZ")
            start = start.replace(tzinfo=pytz.UTC)
            start = start.astimezone(self.timezone)
            start = start.time()
        except:
            start = time(0, 0, 0)
        try:
            # Parse end time
            end = datetime.strptime(end_string, "%Y-%m-%dT%H:%M:%S.%fZ")
            end = end.replace(tzinfo=pytz.UTC)
            end = end.astimezone(self.timezone)
            end = end.time()
        except:
            end = time(23, 59, 59)
        return start, end
