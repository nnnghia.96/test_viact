import cv2
import json
import numpy as np
import os
import imageio
import shutil
import threading
import time
import zipfile
from .zone import Zone
from config import config
from collections import deque
from core.display.utils import draw_detections
from core.logging import logger
from core.workers import TaskScheduler
from core.workers import alarm_queue
from core.workers import queue
from core.workers import video_queue
from core.services import alarm
from core.services import aws
from core.services import dashboard
from core.trackers import AlertObject
from core.trackers import CentroidTracker
from core.trackers import TrackedObject
from easydict import EasyDict
from datetime import datetime
from imutils import paths
from pprint import pprint

class Monitor:

    def __init__(self, name, camera, monitor_config):

        self.debug = monitor_config.get("debug", False)

        self.id = monitor_config.get("id")
        self.name = name
        self.machine_name, self.camera_name = self.name.split("-")
        self.camera = camera

        self.zones = []
        self.engines = set()
        self.zones_config_path = os.path.join("persist", f"{self.name}_zones.json")
        self._load_zones(self.zones_config_path)

        self.tracker = CentroidTracker()
        self.tracked_objects = {}

        self.frame_buffer_lock = threading.Lock()
        self.frame_buffer = deque([], maxlen=100)

        self.snapshot_annotations = {}
        self.snapshot_dir = os.path.join("tmp", f"{self.name}_snapshots")

        # Save snapshot scheduler
        self.save_snaphot = monitor_config.get("save_snaphot", True)
        if self.save_snaphot:
            self.save_snapshot_interval = monitor_config.get("save_snapshot_interval", 600)
            self.save_snapshot_start_hour = monitor_config.get("save_snapshot_start_hour", 0)
            self.save_snapshot_end_hour = monitor_config.get("save_snapshot_end_hour", 23)
            self.snapshot_scheduler = TaskScheduler(
                queue, self._save_snaphot,
                name=f"monitor {self.name} snapshot scheduler",
                interval=self.save_snapshot_interval,
                start_hour=self.save_snapshot_start_hour,
                end_hour=self.save_snapshot_end_hour
            ).start()

        # Upload thumbnail scheduler
        self.thumbnail_scheduler = TaskScheduler(
            queue, self._upload_thumbnail,
            name=f"monitor {self.name} thumbnail scheduler",
            exception_func=self._remove_tmpfiles,
            exception_args=([f"{self.name}.jpg"],),
            interval=1200
        ).start()

        # Update config scheduler
        self.config_scheduler = TaskScheduler(
            queue, self._update_config,
            name=f"monitor {self.name} config scheduler",
            interval=20
        ).start()

        # # Update dynamic danger zone scheduler
        # self.dynamic_danger_zone_scheduler = TaskScheduler(
        #     queue, self._update_dynamic_danger_zone,
        #     name=f"monitor {self.name} dynamic zone scheduler",
        #     interval=0.5
        # ).start()

        # self.dynamic_danger_zone_on = False

        self.recent_alerts = []
        self.last_alert_time = 0
        self.send_alert_interval = monitor_config.get("send_alert_interval", 120)
        self.send_alert_frames = monitor_config.get("send_alert_frames", True)
        self.send_alert_frames_interval = monitor_config.get("send_alert_frames_interval", 5)
        self.send_alert_video = monitor_config.get("send_alert_video", True)

        self.trigger_alarm = monitor_config.get("trigger_alarm", False)
        if self.trigger_alarm:
            self.last_alarm_time = 0
            self.trigger_alarm_interval = monitor_config.get("trigger_alarm_interval", 20)

    def stop(self):
        if self.save_snaphot: self.snapshot_scheduler.stop()
        self.thumbnail_scheduler.stop()
        self.config_scheduler.stop()
        # self.dynamic_danger_zone_scheduler.stop()

    def read(self):
        frame = self.camera.get_frame()
        if frame is None: self._handle_no_frame()
        return frame

    def handle_detections(self, frame, detected_objects=[]):

        self.save_detections(frame, detected_objects)

        # detected_centroids: (cx, cy) -> DetectedObject
        detected_centroids = { obj.centroid: obj for obj in detected_objects }
        # Get centroids of detected objects with 'tracking' enabled
        input_centroids = [obj.centroid for obj in detected_objects]
        # Match detected objects to tracked objects
        centroids = self.tracker.update(input_centroids)
        for id, centroid in centroids.items():
            centroid = tuple(centroid)
            # Get the tracked object
            tracked_object = self.tracked_objects.get(id, None)
            if tracked_object is None:
                tracked_object = TrackedObject(id, centroid)
            tracked_object.centroids.append(centroid)
            if centroid in input_centroids:
                # Get the detected object (synchronize)
                detected_object = detected_centroids[centroid]
                tracked_object.update(detected_object)
            # Only update tracked object that are detected
            tracked_object.detected = centroid in input_centroids
            self.tracked_objects[id] = tracked_object
        # Remove deregistered tracked objects
        [self.tracked_objects.pop(id, None) for id in set(self.tracked_objects).difference(centroids)]

        # Validate all tracked objects
        for zone in self.zones:
            zone.validate(frame.shape[:2], self.tracked_objects.values())

        # tracked_centroids: (cx, cy) -> TrackedObject
        tracked_centroids = { obj.centroid: obj for obj in self.tracked_objects.values() }
        for detected_object in detected_objects:
            #if detected_object.type != "person":
            #    continue
            # Get the tracked object
            tracked_object = tracked_centroids[detected_object.centroid]
            n_trigger_frames = tracked_object.n_trigger_frames

            # Synchronize and Reset tracked object triggered events
            detected_object.update(tracked_object)
            tracked_object.clear()
            # If any events exceed max trigger frames ...
            if any([frames > config.alert.max_trigger_frames for frames in n_trigger_frames.values()]):
                # Get trigger events
                trigger_events = []
                for event, frames in n_trigger_frames.items():
                    if frames > config.alert.max_trigger_frames:
                        trigger_events.append(event)
                        tracked_object.n_trigger_frames[event] = 0

                # Trigger alarm
                if self.trigger_alarm:
                    if self.last_alarm_time == 0 or time.time() - self.last_alarm_time > self.trigger_alarm_interval:
                        logger.info(f"Monitor [{self.name}] Object [{tracked_object.id}] triggered alarm at {time.strftime('%H:%M:%S')}", extra={ "type": "ALARM" })
                        alarm_queue.enqueue(alarm.trigger_alarm, args=(self.camera_name, ), name=f"monitor {self.name} alarm")
                        self.last_alarm_time = time.time()

                # Last alert sent has not exceed time interval, Don't send alert ...
                if self.last_alert_time != 0 and time.time() - self.last_alert_time < self.send_alert_interval:
                    continue

                # The tracked object has exceed max alerts, Don't send alert ...
                if tracked_object.n_trigger_alerts > config.alert.max_trigger_alerts:
                    logger.info(f"Monitor [{self.name}] Object [{tracked_object.id}] exceeded maximum trigger alerts.", extra={ "type": "ALERT" })
                    continue

                # Create an alert object
                alert_object = AlertObject(frame, tracked_object.bbox)
                if alert_object in self.recent_alerts:
                    logger.info(f"Monitor [{self.name}] Object [{tracked_object.id}] already sent identical alert.", extra={ "type": "ALERT" })
                    continue

                # Send alert
                logger.info(f"Monitor [{self.name}] Object [{tracked_object.id}] triggered alert at {time.strftime('%H:%M:%S')}", extra={ "type": "ALERT" })
                if not self.debug:
                    self.send_alert(trigger_events)
                # Reset alert for all tracked objects
                [obj.n_trigger_frames.clear() for obj in self.tracked_objects.values()]
                # Update tracked object trigger alert count
                tracked_object.n_trigger_alerts += 1
                # Update last alert time
                self.last_alert_time = time.time()
                # Update list of recent alerts
                self.recent_alerts.append(alert_object)

        return detected_objects

    def send_alert(self, alert_events, attributes=None):
        date = time.strftime("%Y-%m-%d")
        timestamp = datetime.now().strftime('%Y%m%d-%H%M%S-%f')
        # Alerts have the same filename with .jpg, .mp4, and .zip extensions
        filename = f"{self.machine_name}_{self.camera_name}_{timestamp}"
        url = f"backup/{config.company}/event/{config.mode}/{self.machine_name}/{self.camera_name}/{date}/{filename}"
        # Get alert frames and detections
        self.frame_buffer_lock.acquire()
        alert_frames = list(self.frame_buffer)[-60:]
        self.frame_buffer_lock.release()
        queue.enqueue(
            self._send_alert,
            args=(alert_events, alert_frames, url),
            kwargs={ "attributes": attributes },
            name=f"monitor {self.name} alert",
            max_retries=5,
            exception_func=self._remove_tmpfiles,
            exception_args=([f"{filename}.jpg"], )
        )

    def save_detections(self, frame, detected_objects=[]):
        self.frame_buffer_lock.acquire()
        # Create an ID for each frame (machine_camera_timestamp)
        timestamp = datetime.now().strftime('%Y%m%d-%H%M%S-%f')
        framename = f"{self.machine_name}_{self.camera_name}_{timestamp}"
        self.frame_buffer.append((framename, frame.copy(), detected_objects))
        self.frame_buffer_lock.release()

    #####################################################################################################
    # Scheduler Functions
    #####################################################################################################
    def _upload_thumbnail(self):
        frame = self.read()
        if frame is not None:
            # Write frame to tmp directory
            thumbnail_path = os.path.join("tmp", f"{self.name}.jpg")
            cv2.imwrite(thumbnail_path, frame)
            logger.info(f"Monitor [{self.name}] saved thumbnail to {thumbnail_path}", extra={ "type": "THUMBNAIL" })
            # Upload thumbnail to AWS
            url = f"backup/{config.company}/thumbnail/{config.mode}/{self.name}.jpg"
            image_url = aws.upload_file("viact-deployment-nnt", url, thumbnail_path)
            # Put thumbnail to client dashboard
            dashboard.put_thumbnail(self.id, image_url)
            logger.info(f"Monitor [{self.name}] uploaded thumbnail {thumbnail_path}", extra={ "type": "THUMBNAIL" })

    def _update_config(self):
        config = dashboard.get_monitor_config(self.id)
        if config is not None:
            # Update zones
            self._update_zones(config.get("zone", []))

    def _save_snaphot(self):
        if len(self.frame_buffer) > 0:
            # Save snapshot
            if len(self.snapshot_annotations) == 0:
                os.makedirs(self.snapshot_dir, exist_ok=True)
                logger.info(f"Monitor [{self.name}] created snapshots directory {self.snapshot_dir}", extra={ "type": "SNAPSHOTS" })

            # Obtain frame and annotations
            self.frame_buffer_lock.acquire()
            framename, frame, detected_objects = self.frame_buffer[-1]
            self.frame_buffer_lock.release()

            if framename not in self.snapshot_annotations:
                snapshot_path = os.path.join(self.snapshot_dir, f"{framename}.jpg")
                cv2.imwrite(snapshot_path, frame)
                bboxs = []
                for detected_object in detected_objects:
                    bbox = detected_object.bbox
                    bboxs.append({
                        "category_id": detected_object.type,
                        "bbox": bbox,
                        "confidence": str(detected_object.confidence),
                        "descriptions": detected_object.descriptions
                    })

                self.snapshot_annotations[framename] = {
                    "image_name": f"{framename}.jpg",
                    "annotations": bboxs,
                }

                logger.info(f"Monitor [{self.name}] saved snapshots to {snapshot_path} ... {len(self.snapshot_annotations)}", extra={ "type": "SNAPSHOTS" })

                # Update save interval (Save more snapshots when there are detections)
                new_interval = self.save_snapshot_interval if len(bboxs) == 0 else int(self.save_snapshot_interval // 4)
                self.snapshot_scheduler.set_interval(new_interval)


            if len(self.snapshot_annotations) >= config.snapshots.compress_size:
                # Write JSON file
                snapshots_annotation_path = os.path.join(self.snapshot_dir, "annotations.json")
                data = json.dumps(list(self.snapshot_annotations.values()))
                file = open(snapshots_annotation_path, "w")
                file.write(data)
                file.close()

                # Compress directory
                timestamp = datetime.now().strftime('%Y%m%d-%H%M%S-%f')
                snapshots_filename = f"{self.machine_name}_{self.camera_name}_{timestamp}"
                snapshots_compressed_path = os.path.join("tmp", f"{snapshots_filename}.zip")
                self._make_zipfile(snapshots_compressed_path, self.snapshot_dir)
                logger.info(f"Monitor [{self.name}] compressed snapshots to {snapshots_compressed_path}", extra={ "type": "SNAPSHOTS" })

                # Clear annotations
                shutil.rmtree(self.snapshot_dir)
                self.snapshot_annotations = {}

                # Upload annotations
                queue.enqueue(
                    self._upload_snapshots,
                    args=(snapshots_compressed_path, ),
                    name=f"monitor {self.name} snapshots"
                )

    def _update_dynamic_danger_zone(self):
        if self.dynamic_danger_zone_on:
            try:
                # Obtain frame and annotations
                self.frame_buffer_lock.acquire()
                framename, frame, detected_objects = self.frame_buffer[-1]
                self.frame_buffer_lock.release()

                zone_config = self.dynamic_danger_zone_config
                recent_detected_bboxes = [detected_object.bbox for detected_object in detected_objects if detected_object.type == "traffic cone"]
                self.zones = [zone for zone in self.zones for engine in zone.engines if engine != "danger-zone"]
                if len(recent_detected_bboxes) > 10:
                    recent_detected_bboxes = recent_detected_bboxes[:10]
                if len(recent_detected_bboxes) >= 3:
                    zone = [( (x1 + x2) / 2 / self.camera.shape[1] * zone_config["width"], 
                               y2 / self.camera.shape[0] * zone_config["height"] ) for x1, y1, x2, y2 in recent_detected_bboxes]
                    zone = self._cart2pol_sort(zone)
                    zone_config["polygon"] = zone
                    zone_config["dynamic"] = True
                    self.zones.append(Zone("polygon", EasyDict(zone_config)))
                    self.engines.add("danger-zone")
                else:
                    pass
            except Exception as e:
                logger.info(f"Error in dynamic danger zone! {e}", extra={ "type": "DEBUG" })

    #####################################################################################################
    # Upload Functions
    #####################################################################################################
    def _send_alert(self, alert_events, alert_frames, url, attributes=None):
        # Create alert image
        filename = url.split(os.path.sep)[-1]
        alert_image_path = os.path.join("tmp", f"{filename}.jpg")
        if not os.path.exists(alert_image_path):
            alert_framename, alert_frame, alert_detection = alert_frames[-5]
            cv2.imwrite(alert_image_path, draw_detections(alert_frame.copy(), alert_detection))
            logger.info(f"Monitor [{self.name}] saved alert image to {alert_image_path}... ", extra={ "type": "ALERT" })

        # Upload image
        image_url = aws.upload_file("viact-deployment-nnt", f"{url}.jpg", alert_image_path)
        # Send alerts
        alert_ids = []
        for event in alert_events:
            response = dashboard.post_alert(self.id, event, image_url=image_url, attributes=attributes)
            logger.info(f"Monitor [{self.name}] sent alert {event} ... {response.get('id')}", extra={ "type": "ALERT" })
            alert_ids.append(response.get("id"))
        # Send alert videos
        if self.send_alert_video:
            video_queue.enqueue(
                self._upload_alert_video,
                args=(alert_ids, alert_frames, url),
                name=f"monitor {self.name} alert",
                max_retries=5,
                exception_func=self._remove_tmpfiles,
                exception_args=([f"{filename}.mp4", f"{filename}.zip"], )
            )

    def _upload_alert_video(self, alert_ids, alert_frames, url):
        # Create alert video
        filename = url.split(os.path.sep)[-1]
        alert_video_path = os.path.join("tmp", f"{filename}.mp4")
        alert_compressed_path = os.path.join("tmp", f"{filename}.zip")

        if not os.path.exists(alert_video_path):
            logger.info(f"Monitor [{self.name}] saved alert video to {alert_video_path}... ", extra={ "type": "ALERT" })

            # Video writer
            # fourcc = cv2.VideoWriter_fourcc(*config.alert.video_encoder)
            alert_frame = alert_frames[-1][1]
            frame_h, frame_w = alert_frame.shape[:2]
            # writer = cv2.VideoWriter(alert_video_path, cv2.VideoWriter_fourcc(*'mp4v'), 30, (frame_w, frame_h))
            #writer1 = cv2.VideoWriter("tmp/out.mp4", -1, 1, (frame_w, frame_h))
            writer = imageio.get_writer(alert_video_path, fps=30)
            # Alert frames annotations
            if self.send_alert_frames:
                alert_dir = os.path.join("tmp", filename)
                os.makedirs(alert_dir, exist_ok=True)
                annotations = []
                logger.info(f"Monitor [{self.name}] created alert directory {alert_dir}", extra={ "type": "ALERT" })

            # Write alert video
            for i, (framename, frame, detected_objects) in enumerate(alert_frames):
                print(i)
                # Save alert frame + annotation
                if self.send_alert_frames and (i % self.send_alert_frames_interval) == 0:
                    alert_frame_path = os.path.join(alert_dir, f"{framename}.jpg")
                    cv2.imwrite(alert_frame_path, frame)
                    # Save bounding box annotations
                    bboxs = []
                    for detected_object in detected_objects:
                        bbox = detected_object.bbox
                        bboxs.append({
                            "category_id": detected_object.type,
                            "bbox": bbox,
                            "confidence": str(detected_object.confidence),
                            "descriptions": detected_object.descriptions
                        })
                    # Save annotations
                    annotations.append({
                        "image_name": f"{framename}.jpg",
                        "annotations": bboxs
                    })
                    logger.info(f"Monitor [{self.name}] saved alert frame to {alert_frame_path}", extra={ "type": "ALERT" })

                # Draw bounding boxes on frames
                writer.append_data(cv2.cvtColor(draw_detections(frame.copy(), detected_objects), cv2.COLOR_BGR2RGB))
                #writer.write(draw_detections(frame.copy(), detected_objects))
                #writer1.write(draw_detections(frame.copy(), detected_objects))
            #writer.release()
            #writer1.release()
            writer.close()

            # Create annotation file
            if self.send_alert_frames:
                print("----------")
                alert_annotation_path = os.path.join(alert_dir, "annotations.json")
                data = json.dumps(annotations)
                file = open(alert_annotation_path, "w")
                file.write(data)
                file.close()

                self._make_zipfile(alert_compressed_path, alert_dir)
                shutil.rmtree(alert_dir)
                logger.info(f"Monitor [{self.name}] compressed alert frames to {alert_compressed_path}", extra={ "type": "ALERT" })

        video_url = aws.upload_file("viact-deployment-nnt", f"{url}.mp4", alert_video_path)
        for alert_id in alert_ids:
            response = dashboard.update_alert(alert_id, video_url=video_url)
            logger.info(f"Monitor [{self.name}] uploaded video {alert_video_path} to alert id {alert_id} ... {response['message']}", extra={ "type": "ALERT" })

        # Upload annotation file to AWS
        if self.send_alert_frames:
            aws.upload_file("viact-deployment-nnt", f"{url}.zip", alert_compressed_path)
            logger.info(f"Monitor [{self.name}] uploaded alert frames {alert_compressed_path}", extra={ "type": "ALERT" })

    def _upload_snapshots(self, snapshots_compressed_path):
        date = time.strftime("%Y-%m-%d")
        snapshots_filename = os.path.splitext(os.path.basename(snapshots_compressed_path))[0]
        url = f"backup/{config.company}/snapshots/{config.mode}/{self.machine_name}/{self.camera_name}/{date}/{snapshots_filename}"
        aws.upload_file("viact-deployment-nnt", f"{url}.zip", snapshots_compressed_path)
        logger.info(f"Monitor [{self.name}] uploaded snapshots {snapshots_compressed_path}", extra={ "type": "SNAPSHOTS" })

    #####################################################################################################
    # Helper Functions
    #####################################################################################################
    def _make_zipfile(self, path, directory):
        with zipfile.ZipFile(path, 'w', zipfile.ZIP_DEFLATED) as zip:
            for root, dirs, files in os.walk(directory):
                for file in files:
                    zip.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), directory))

    def _remove_tmpfiles(self, files):
        for file in files:
            tmp_path = os.path.join("tmp", file)
            if os.path.exists(tmp_path):
                if os.path.isfile(tmp_path): os.remove(tmp_path)
                if os.path.isdir(tmp_path): shutil.rmtree(tmp_path)
        logger.info(f"Monitor [{self.name}] removed files {', '.join(files)}")

    def _load_zones(self, zones_config_path):
        if os.path.exists(zones_config_path):
            try:
                zone_configs = json.load(open(zones_config_path, "r"))
                self._update_zones(zone_configs)
                logger.info(f"Monitor [{self.name}] loaded zones from {zones_config_path}")
            except:
                logger.info(f"Monitor [{self.name}] failed to load zones from {zones_config_path}", extra={ "type": "WARNING" })

    def _update_zones(self, zone_configs):
        # self.dynamic_danger_zone_on = False
        zones = []
        for zone_config in zone_configs:
            # if zone_config["engines"] == "danger-zone":
            #     self.dynamic_danger_zone_on = True
            #     self.dynamic_danger_zone_config = zone_config
            if zone_config.get("circle", False):
                zones.append(Zone("circle", EasyDict(zone_config)))
            elif zone_config.get("square", False):
                zones.append(Zone("square", EasyDict(zone_config)))
            elif zone_config.get("polygon", False):
                zones.append(Zone("polygon", EasyDict(zone_config)))
        # Update engines
        self.zones = zones
        self.engines = set([engine for zone in self.zones for engine in zone.engines])
        # Save zones to file
        data = json.dumps(zone_configs)
        file = open(self.zones_config_path, "w")
        file.write(data)
        file.close()

    def _handle_no_frame(self):
        self.camera.no_frames += 1
        if self.camera.no_frames > self.camera.max_no_frames:
            logger.info(f"Camera [{self.camera.name}] cannot read frame.", extra={ "type": "ALERT" })
            logger.info(f"Camera [{self.camera.name}] trying to reconnect ...", extra={ "type": "ALERT" })
            queue.enqueue(
                dashboard.post_alert,
                args=(self.id, "unstable-stream"),
                name=f"monitor {self.name} alert",
                max_retries=0
            )
            self.camera.reconnect()
            self.camera.no_frames = 0

    def _cart2pol_sort(self, zone):
        cx, cy = 0, 0
        for (x, y) in zone:
            cx += x
            cy += y
        cx /= len(zone)
        cy /= len(zone)
        centred_zone = [(x-cx, y-cy) for (x, y) in zone]
        zone = [(x, y, np.arctan2(cy, cx)) for (x, y), (cx, cy) in zip(zone, centred_zone)]
        zone.sort(key=lambda x: x[-1])
        zone = [{"x": x, "y": y} for (x, y, phi) in zone]
        return zone
