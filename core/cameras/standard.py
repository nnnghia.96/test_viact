import imutils
from .camera import Camera

class StandardCamera(Camera):

    def __init__(self, url, name="Standard Dummy"):
        super(StandardCamera, self).__init__(url, name)
        self.read()

    def read(self):
        self.frame = self.stream.read()
        if self.frame is not None:
            self.frame = imutils.resize(self.frame, width=1280)

    def get_frame(self):
        return self.frame
