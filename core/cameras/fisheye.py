import cv2
import imutils
import numpy as np
from .camera import Camera

class FisheyeCamera(Camera):

    def __init__(self, url, name="Fisheye Dummy"):
        super(FisheyeCamera, self).__init__(url, name)
        self._build_maps()

        self.n_screens = 4
        self.screen_w = int(self.dest_h * 16 / 9)

    def read(self):
        self.frame = self.stream.read()
        if self.frame is not None:
            self.frame = cv2.remap(self.frame, self.xmap, self.ymap, cv2.INTER_LINEAR)

    def get_frame(self, direction=0):
        if self.frame is None: return None
        start = int(direction * self.screen_w)
        end = int(start + self.screen_w) % self.dest_w
        # Get frame segment
        frame = self.frame[:, start:end]
        if start > end:
            frame = np.concatenate((self.frame[:, start:], self.frame[:, :end]), axis=1)
        # return imutils.resize(frame, width=1280)
        return frame

    def _build_maps(self):
        frame = self.stream.read()
        if frame is not None:
            self.r1 = 300
            self.r2 = 950
            # Source width and height
            source_h, source_w = frame.shape[:2]
            self.cx, self.cy = source_w // 2, source_h // 2
            # Destination width and height
            self.dest_h = self.r2 - self.r1
            self.dest_w = int(2 * np.pi * (self.r1 + self.r2) / 2)
            xmap = np.zeros((self.dest_h, self.dest_w), np.float32)
            ymap = np.zeros((self.dest_h, self.dest_w), np.float32)
            for y in range(self.dest_h - 1):
                for x in range(self.dest_w - 1):
                    r = self.r1 + float(self.dest_h) - float(y)
                    theta = float(x) / float(self.dest_w) * 2 * np.pi
                    xmap.itemset((y, x), int(self.cx + r * np.sin(theta)))
                    ymap.itemset((y, x), int(self.cy + r * np.cos(theta)))
            self.xmap = xmap
            self.ymap = ymap
