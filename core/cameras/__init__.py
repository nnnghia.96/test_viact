from .standard import StandardCamera
from .fisheye import FisheyeCamera
from .video_file import VideoFile

camera_factory = {
    "file": VideoFile,
    "standard": StandardCamera,
    "fisheye": FisheyeCamera,
}
