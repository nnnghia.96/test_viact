import cv2

class VideoFile:

    def __init__(self, url, name="Video File"):
        self.name = name
        self.url = url
        self.capture = cv2.VideoCapture(self.url)
        # Current frame
        self.frame = None

        self.read()

    def read(self):
        ret, frame = self.capture.read()
        # print(ret)
        while not ret:
            self.capture.set(cv2.CAP_PROP_POS_FRAMES, 0)
            ret, frame = self.capture.read()
        self.frame = frame

    def get_frame(self):
        return self.frame

    def stop(self):
        self.capture.release()
