import cv2
import time
from config import config
from imutils.video import VideoStream

class Camera:

    def __init__(self, url, name="Dummy"):
        self.name = name
        self.url = url
        # Current frame
        self.frame = None
        # Number of disconnected frames.
        self.no_frames = 0
        self.max_no_frames = config.alert.max_no_frames

        self.start()

    def stop(self):
        self.stream.stop()
        time.sleep(2)

    def start(self):
        self.stream = VideoStream(src=self.url).start()

    def reconnect(self):
        self.stop()
        self.start()
