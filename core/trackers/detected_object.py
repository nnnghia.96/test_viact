class DetectedObject:

    def __init__(self, bbox, confidence, type, descriptions={}, tracking=True):
        # Bounding box
        self.bbox = bbox
        self.confidence = confidence
        # Centroid
        x1, y1, x2, y2 = bbox
        cx = int((x1 + x2) / 2)
        cy = int((y1 + y2) / 2)
        self.centroid = (cx, cy)
        # Object information
        self.type = type
        self.descriptions = descriptions

    def update(self, tracked_object):
        # Trigger events
        self.trigger_events = tracked_object.trigger_events
        self.n_trigger_alerts = tracked_object.n_trigger_alerts
        self.n_trigger_frames = tracked_object.n_trigger_frames

    def __repr__(self):
        return f"Detected Object ({self.type})"
