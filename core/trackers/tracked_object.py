from collections import defaultdict
from collections import deque

class TrackedObject:

    def __init__(self, id, centroid):
        self.id = id
        # Position
        self.start = centroid
        self.centroids = deque([], maxlen=20)
        # Trigger events
        self.detected = False
        self.trigger_events = []
        self.n_trigger_frames = defaultdict(int)
        self.n_trigger_alerts = 0

    def clear(self):
        self.direction = None
        self.trigger_events = []

    def update(self, detected_object):
        self.bbox = detected_object.bbox
        self.centroid = detected_object.centroid
        self.type = detected_object.type
        self.descriptions = detected_object.descriptions

    def add_trigger_event(self, event):
        self.trigger_events.append(event)
        self.n_trigger_frames[event] += 1

    def __repr__(self):
        return f"Tracked Object ({self.type}) at {self.centroid}"
