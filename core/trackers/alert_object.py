import cv2
import numpy as np
import time

class AlertObject:

    def __init__(self, frame, bbox):
        # Save bounding box
        self.bbox = bbox
        # Extract object region
        x1, y1, x2, y2 = bbox
        region = frame[y1:y2, x1:x2]
        self.signal = np.abs(np.fft.fft2(cv2.resize(region, (10, 10))))
        # Alert time
        self.alert_time = time.time()

    @staticmethod
    def cosine_distance(signal1, signal2):
        signal1 = signal1.flatten()
        signal2 = signal2.flatten()
        return np.dot(signal1, signal2) / (np.linalg.norm(signal1) * np.linalg.norm(signal2) + 1e-8)

    @staticmethod
    def iou(bbox1, bbox2):
        x1 = max(bbox1[0], bbox2[0])
        y1 = max(bbox1[1], bbox2[1])
        x2 = min(bbox1[2], bbox2[2])
        y2 = min(bbox1[3], bbox2[3])

        inter_area = max(0, x2 - x1 + 1) * max(0, y2 - y1 + 1)
        bbox1_area = (bbox1[2] - bbox1[0] + 1) * (bbox1[3] - bbox1[1] + 1)
        bbox2_area = (bbox2[2] - bbox2[0] + 1) * (bbox2[3] - bbox2[1] + 1)
        iou = inter_area / float(bbox1_area + bbox2_area - inter_area)
        return iou

    def __eq__(self, other):
        if isinstance(other, AlertObject):
            if AlertObject.iou(self.bbox, other.bbox) < 0.95:
                return False
            if AlertObject.cosine_distance(self.signal, other.signal) < 0.95:
                return False
            return True

        return False
