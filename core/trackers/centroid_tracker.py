import numpy as np
from collections import OrderedDict
from scipy.spatial import distance

class CentroidTracker:

    def __init__(self):
        # Tracking ID
        self.next_id = 0
        # Key: Object ID. Value: Object centroid
        self.objects = OrderedDict()
        # Key: Object ID. Value: Consecutive frame the object disappeared
        self.disappeared = OrderedDict()
        self.max_disappear_frames = 50
        # Distance between centroids to consider as new object
        self.max_distance = 250

    def _register(self, centroid):
        self.objects[self.next_id] = centroid
        self.disappeared[self.next_id] = 0
        # Reuse old id when ID exceeded 10000
        self.next_id = (self.next_id + 1) % 10000

    def _deregister(self, id):
        del self.objects[id]
        del self.disappeared[id]

    def update(self, input_centroids):
        if len(input_centroids) == 0:
            # Increment disappear frames for each object
            for id in list(self.disappeared.keys()):
                self.disappeared[id] += 1
                # Deregister obeject if disappear for long time
                if self.disappeared[id] > self.max_disappear_frames:
                    self._deregister(id)
            return self.objects

        input_centroids = np.array(input_centroids)

        # If there are no tracking objects ...
        if len(self.objects) == 0:
            for centroid in input_centroids:
                self._register(centroid)
        # Otherwise, map objects to closest centroids ...
        else:
            tracking_ids = list(self.objects.keys())
            tracking_centroids = list(self.objects.values())

            # Calculate distance matrix between tracking centroids and input centroids
            D = distance.cdist(np.array(tracking_centroids), input_centroids)
            rows = D.min(axis=1).argsort()
            cols = D.argmin(axis=1)[rows]

            used_rows = set()
            used_cols = set()
            for row, col in zip(rows, cols):
                if row in used_rows or col in used_cols:
                    continue
                if D[row, col] > self.max_distance:
                    continue
                self.objects[tracking_ids[row]] = input_centroids[col]
                self.disappeared[tracking_ids[row]] = 0
                used_rows.add(row)
                used_cols.add(col)

            unused_rows = set(range(0, D.shape[0])).difference(used_rows)
            unused_cols = set(range(0, D.shape[1])).difference(used_cols)
            # Some tracking objects disappeared
            if D.shape[0] > D.shape[1]:
                for row in unused_rows:
                    self.disappeared[tracking_ids[row]] += 1
                    if self.disappeared[tracking_ids[row]] > self.max_disappear_frames:
                        self._deregister(tracking_ids[row])
            # Some new centroids appeared
            for col in unused_cols:
                self._register(input_centroids[col])

        return self.objects
