import cv2
import numpy as np
from .model import Model

class Classifier(Model):

    def __init__(self, engine, input_dim, classes):
        super(Classifier, self).__init__(engine)
        self.input_w, self.input_h = input_dim
        self.classes = classes
        self.n_classes = len(classes)

    def infer(self, image):
        input = self._preprocess(image)
        predictions = self.inference(input)
        return self._postprocess(predictions)

    def _preprocess(self, image):
        image = cv2.resize(image, (self.input_w, self.input_h))
        image = np.expand_dims(image.transpose(2, 0, 1), axis=0)
        return image

    def _postprocess(self, predictions):
        predictions = predictions[0]
        return self.classes[predictions.argmax()]
