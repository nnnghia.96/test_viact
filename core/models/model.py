import numpy as np
import pycuda.autoinit
import pycuda.driver as cuda
import tensorrt as trt

class HostDeviceMem:
    def __init__(self, host_mem, device_mem):
        self.host = host_mem
        self.device = device_mem

class Model:

    def __init__(self, engine):
        self.engine = engine
        self.stream = cuda.Stream()
        self.context = engine.create_execution_context()
        self._allocate_buffers(engine)

    def inference(self, input):
        np.copyto(self.inputs[0].host, input.ravel())
        [cuda.memcpy_htod_async(input.device, input.host, self.stream) for input in self.inputs]
        self.context.execute_async_v2(bindings=self.bindings, stream_handle=self.stream.handle)
        [cuda.memcpy_dtoh_async(output.host, output.device, self.stream) for output in self.outputs]
        self.stream.synchronize()
        return [output.host for output in self.outputs]

    def _allocate_buffers(self, engine):
        self.inputs = []
        self.outputs = []
        self.bindings = []
        for binding in engine:
            size = trt.volume(engine.get_binding_shape(binding))
            dtype = trt.nptype(engine.get_binding_dtype(binding))
            # Allocate host and device buffers
            host_mem = cuda.pagelocked_empty(size, dtype)
            device_mem = cuda.mem_alloc(host_mem.nbytes)
            # Append device buffer to device bindings
            self.bindings.append(int(device_mem))
            if engine.binding_is_input(binding):
                self.inputs.append(HostDeviceMem(host_mem, device_mem))
            else:
                self.outputs.append(HostDeviceMem(host_mem, device_mem))
