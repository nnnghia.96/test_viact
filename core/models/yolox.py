import cv2
import onnxruntime
import torch
import torchvision
import numpy as np
from .model import Model

class YOLOX(Model):
    def __init__(self, engine, input_dim, classes):
        super(YOLOX, self).__init__(engine)
        self.input_w, self.input_h = input_dim
        self.classes = classes
        self.num_classes = len(classes)
        #self.session = onnxruntime.InferenceSession("onnx/yolox_s.onnx", providers=['CUDAExecutionProvider', 'CPUExecutionProvider'])
        #self.input_name = self.session.get_inputs()[0].name
        self.fm_strides = [8, 16, 32]
        self.fm_sizes = [[self.input_h / stride, self.input_w / stride] for stride in self.fm_strides]
        grids = []
        strides = []
        for (fm_h, fm_w), stride in zip(self.fm_sizes, self.fm_strides):
            yv, xv = torch.meshgrid(torch.arange(fm_h), torch.arange(fm_w))
            grid = torch.stack((xv, yv), 2).reshape(1, -1, 2)

            grids.append(grid)
            shape = grid.shape[:2]
            strides.append(torch.full((*shape, 1), stride))
        self.grids = torch.cat(grids, dim=1)
        self.strides = torch.cat(strides, dim=1)

    def infer(self, image):
        input = self._preprocess(image)
        #input, ratio = self._preprocess(image)
        prediction = self.inference(input)
        #prediction = self.session.run(None, {self.input_name: input[None, :, :, :]})[0]
        #return self._postprocess(prediction, self.n_classes, ratio)
        return self._postprocess(prediction, image)

    def _preprocess(self, image):
        #padded = np.ones((self.input_h, self.input_w, 3), dtype=np.uint8)*114
        #r = min(self.input_h/image.shape[0], self.input_w/image.shape[1])
        #resized = cv2.resize(image, (int(image.shape[1]*r), int(image.shape[0]*r)), interpolation=cv2.INTER_LINEAR).astype(np.uint8)
        #padded[:int(image.shape[0]*r), :int(image.shape[1]*r)] = resized
        #padded = padded.transpose(2, 0, 1)
        #out = np.ascontiguousarray(padded, dtype=np.float32)
        #return out, r
        image = cv2.resize(image, (self.input_w, self.input_h))
        return np.expand_dims(image.transpose(2, 0, 1), 0)

    #def _nms(self, boxes, scores, nms_thr):
    #    """Single class NMS implemented in Numpy."""
    #    x1 = boxes[:, 0]
    #    y1 = boxes[:, 1]
    #    x2 = boxes[:, 2]
    #    y2 = boxes[:, 3]

    #    areas = (x2-x1+1)*(y2-y1+1)
    #    order = scores.argsort()[::-1]

    #    keep = []
    #    while order.size > 0:
    #        i = order[0]
    #        keep.append(i)
    #        xx1 = np.maximum(x1[i], x1[order[1:]])
    #        yy1 = np.maximum(y1[i], y1[order[1:]])
    #        xx2 = np.minimum(x2[i], x2[order[1:]])
    #        yy2 = np.minimum(y2[i], y2[order[1:]])

    #        w = np.maximum(0.0, xx2-xx1+1)
    #        h = np.maximum(0.0, yy2-yy1+1)
    #        inter = w*h
    #        ovr = inter/(areas[i]+areas[order[1:]]-inter)

    #        inds = np.where(ovr <= nms_thr)[0]
    #        order = order[inds+1]

    #    return keep

    #def _postprocess(self, prediction, num_classes, ratio, score_thr=0.1, nms_thr=0.45, class_agnostic=False):
        #grids, expanded_strides = [], []
        #strides = [8, 16, 32]
        #hsizes = [self.input_h//s for s in strides]
        #wsizes = [self.input_w//s for s in strides]
        #for hs, ws, s in zip(hsizes, wsizes, strides):
        #    xv, yv = np.meshgrid(np.arange(ws), np.arange(hs))
        #    g = np.stack((xv, yv), 2).reshape(1, -1, 2)
        #    grids.append(g)
        #    shape = g.shape[:2]
        #    expanded_strides.append(np.full((*shape, 1), s))
        #grids = np.concatenate(grids, 1)
        #expanded_strides = np.concatenate(expanded_strides, 1)
        #prediction[..., :2] = (prediction[..., :2]+grids)*expanded_strides
        #prediction[..., 2:4] = np.exp(prediction[..., 2:4])*expanded_strides
        
        #boxes = prediction[0][:, :4]
        #scores = prediction[0][:, 4:5]*prediction[0][:, 5:]
        #boxes_xyxy = np.ones_like(boxes)
        #boxes_xyxy[:, 0] = boxes[:, 0]-boxes[:, 2]/2.
        #boxes_xyxy[:, 1] = boxes[:, 1]-boxes[:, 3]/2.
        #boxes_xyxy[:, 2] = boxes[:, 0]+boxes[:, 2]/2.
        #boxes_xyxy[:, 3] = boxes[:, 1]+boxes[:, 3]/2.
        #boxes_xyxy /= ratio

        #cls_inds = scores.argmax(1)
        #cls_scores = scores[np.arange(len(cls_inds)), cls_inds]
        #valid_score_mask = cls_scores > score_thr
        #if valid_score_mask.sum() == 0:
        #    return [], [], []
        #valid_scores = cls_scores[valid_score_mask]
        #valid_boxes = boxes_xyxy[valid_score_mask]
        #valid_cls_inds = cls_inds[valid_score_mask]

        #is_person_mask = valid_cls_inds == 0
        #valid_scores = valid_scores[is_person_mask]
        #valid_boxes = valid_boxes[is_person_mask]
        #valid_cls_inds = valid_cls_inds[is_person_mask]

        #keep = self._nms(valid_boxes, valid_scores, nms_thr)
        #if keep:
        #    return valid_boxes[keep], valid_scores[keep], valid_cls_inds[keep]
        #else:
        #    return [], [], []
    
    def _postprocess(self, outputs, original_image, conf_thres=0.45, nms_thres=0.3):
        outputs = torch.from_numpy(np.array(outputs))
        outputs = outputs.reshape(1, -1, self.num_classes + 5)
        
        outputs[..., :2] = (outputs[..., :2] + self.grids) * self.strides
        outputs[..., 2:4] = torch.exp(outputs[..., 2:4]) * self.strides

        box_corner = outputs.new(outputs.shape)
        box_corner[:, :, 0] = outputs[:, :, 0] - outputs[:, :, 2] / 2
        box_corner[:, :, 1] = outputs[:, :, 1] - outputs[:, :, 3] / 2
        box_corner[:, :, 2] = outputs[:, :, 0] + outputs[:, :, 2] / 2
        box_corner[:, :, 3] = outputs[:, :, 1] + outputs[:, :, 3] / 2
        outputs[:, :, :4] = box_corner[:, :, :4]

        image_pred = outputs[0]

        class_conf, class_pred = torch.max(
            image_pred[:, 5 : 5 + self.num_classes], 1, keepdim=True
        )
        conf_mask = (image_pred[:, 4] * class_conf.squeeze() >= conf_thres).squeeze()
        detections = torch.cat((image_pred[:, :5], class_conf, class_pred.float()), 1)
        detections = detections[conf_mask]
        nms_out_index = torchvision.ops.batched_nms(
            detections[:, :4],
            detections[:, 4] * detections[:, 5],
            torch.zeros_like(detections[:, 6]),
            nms_thres,
        )

        detections = detections[nms_out_index].numpy()
        
        ori_h, ori_w = original_image.shape[:2]
        detections[:, 0] = list(map(lambda x: max(0, x), detections[:, 0] / self.input_w * ori_w))
        detections[:, 1] = list(map(lambda y: max(0, y), detections[:, 1] / self.input_h * ori_h))
        detections[:, 2] = list(map(lambda x: min(x, ori_w), detections[:, 2] / self.input_w * ori_w))
        detections[:, 3] = list(map(lambda y: min(y, ori_h), detections[:, 3] / self.input_h * ori_h))
        
        return (detections[:, :5], list(map(lambda x: self.classes[x], detections[:, 6].astype("int"))))
        




        
