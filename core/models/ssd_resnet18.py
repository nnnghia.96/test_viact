import cv2
import itertools
import numpy as np
from .model import Model
from easydict import EasyDict
from math import ceil

class SSDResnet18(Model):

    config = EasyDict({
        "architecture": "ssd",
        "steps": [8, 16, 32],
        "min_sizes": [[16, 32], [64, 128], [256, 512]],
        "variances": [0.1, 0.2],

        "obj_thresh": 0.5,
        "nms_thresh": 0.3,
        "top_k": 100,
    })

    def __init__(self, engine, input_dim, classes):
        super(SSDResnet18, self).__init__(engine)
        self.input_w, self.input_h = input_dim
        self.classes = classes
        self.n_classes = len(classes)
        self.priors = self._create_prior_boxes()

    def infer(self, image):
        input = self._preprocess(image)
        predictions = self.inference(input)
        return self._postprocess(image, predictions)

    def _preprocess(self, image):
        image = cv2.resize(image, (self.input_w, self.input_h))
        image = np.expand_dims(image.transpose(2, 0, 1), axis=0)
        return image

    def _postprocess(self, image, predictions):
        predict_locs, predict_confs = predictions
        image_height, image_width = image.shape[:2]

        predict_locs = predict_locs.reshape(1, -1, 4)
        predict_bboxs = self._gcxgcy_to_xyxy(np.squeeze(predict_locs, axis=0))
        predict_bboxs *= np.array([image_width, image_height, image_width, image_height])

        predict_confs = predict_confs.reshape(1, -1, self.n_classes + 1)
        predict_confs = np.squeeze(predict_confs, axis=0)[:, 1:]
        predict_labels = np.array(predict_confs.argmax(axis=1))
        predict_scores = predict_confs.max(axis=1)

        exceed_obj_thresh = np.where(predict_scores > self.config.obj_thresh)
        predict_bboxs = predict_bboxs[exceed_obj_thresh]
        predict_labels = predict_labels[exceed_obj_thresh]
        predict_scores = predict_scores[exceed_obj_thresh]

        # Sort bbox by confidence before performing NMS
        order = predict_scores.argsort()[::-1]
        predict_bboxs = predict_bboxs[order]
        predict_labels = predict_labels[order]
        predict_scores = predict_scores[order]

        predict_bboxs_scores = np.hstack((predict_bboxs, predict_scores[:,np.newaxis]))
        predict_bboxs_scores = predict_bboxs_scores.astype(np.float32, copy=False)
        exceed_nms_thresh = self._nms(predict_bboxs_scores, self.config.nms_thresh)

        predict_bboxs_scores = predict_bboxs_scores[exceed_nms_thresh]
        predict_labels = predict_labels[exceed_nms_thresh]

        predict_bboxs_scores = predict_bboxs_scores[:self.config.top_k]
        # Convert labels to string
        predict_labels = predict_labels[:self.config.top_k]
        predict_labels = [self.classes[i] for i in predict_labels]
        return predict_bboxs_scores, predict_labels

    def _nms(self, predict_bboxs_scores, nms_thresh):
        x1 = predict_bboxs_scores[:, 0]
        y1 = predict_bboxs_scores[:, 1]
        x2 = predict_bboxs_scores[:, 2]
        y2 = predict_bboxs_scores[:, 3]
        scores = predict_bboxs_scores[:, 4]

        areas = (x2 - x1 + 1) * (y2 - y1 + 1)
        order = scores.argsort()[::-1]

        keep = []
        while order.size > 0:
            i = order[0]
            keep.append(i)
            xx1 = np.maximum(x1[i], x1[order[1:]])
            yy1 = np.maximum(y1[i], y1[order[1:]])
            xx2 = np.minimum(x2[i], x2[order[1:]])
            yy2 = np.minimum(y2[i], y2[order[1:]])

            w = np.maximum(0.0, xx2 - xx1 + 1)
            h = np.maximum(0.0, yy2 - yy1 + 1)
            inter = w * h
            ovr = inter / (areas[i] + areas[order[1:]] - inter)

            inds = np.where(ovr <= nms_thresh)[0]
            order = order[inds + 1]

        return keep

    def _gcxgcy_to_xyxy(self, gcxgcy):
        xyxy = np.c_[
            self.priors[:,:2] + gcxgcy[:,:2] * self.config.variances[0] * self.priors[:,2:],
            self.priors[:,2:] * np.exp(gcxgcy[:,2:] * self.config.variances[1])
        ]
        xyxy[:,:2] -= xyxy[:,2:] / 2
        xyxy[:,2:] += xyxy[:,:2]
        xyxy = np.clip(xyxy, 0, 1)
        return xyxy

    def _create_prior_boxes(self):
        prior_boxes = []
        dims = [[ceil(self.input_h / step), \
                 ceil(self.input_w / step)] for step in self.config.steps]
        for k, dim in enumerate(dims):
            min_sizes = self.config.min_sizes[k]
            for i, j in itertools.product(range(dim[0]), range(dim[1])):
                for min_size in min_sizes:
                    s_kx = min_size / self.input_w
                    s_ky = min_size / self.input_h
                    dense_cx = [x * self.config.steps[k] / self.input_w for x in [j + 0.5]]
                    dense_cy = [y * self.config.steps[k] / self.input_h for y in [i + 0.5]]
                    for cy, cx in itertools.product(dense_cy, dense_cx):
                        prior_boxes += [cx, cy, s_kx, s_ky]
        prior_boxes = np.array(prior_boxes).reshape((-1, 4))
        return prior_boxes
