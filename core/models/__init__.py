from .classifier import Classifier
from .yolox import YOLOX

architectures = {
    "yolox": YOLOX,
    "classifier": Classifier,
}
