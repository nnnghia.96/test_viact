import cv2
import numpy as np
from .model import Model

class EfficientnetB0(Model):

    def __init__(self, engine, input_dim, classes):
        super(EfficientnetB0, self).__init__(engine)
        self.input_w, self.input_h = input_dim
        self.classes = classes
        self.n_classes = len(classes)

    def infer(self, image):
        input = self._preprocess(image)
        predictions = self.inference(input)
        return self._postprocess(predictions)

    def _preprocess(self, image):
        image = cv2.resize(image, (self.input_w, self.input_h))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = image.astype(np.float32) / 255.
        image = np.expand_dims(image, axis=0)
        return image

    def _postprocess(self, predictions):
        predictions = predictions[0]
        return self.classes[predictions.argmax()]
