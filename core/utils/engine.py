import os
import tensorrt as trt
from core.services import aws

TRT_LOGGER = trt.Logger(trt.Logger.WARNING)

def load_trt(filepath):
    with open(filepath, "rb") as file, trt.Runtime(TRT_LOGGER) as runtime:
        engine = runtime.deserialize_cuda_engine(file.read())
        return engine

def load_onnx(filepath):

    # Builder
    builder = trt.Builder(TRT_LOGGER)
    builder_config = builder.create_builder_config()

    # Use explicit batch
    network_flags = 0
    network_flags = 1 << (int)(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)
    network = builder.create_network(network_flags)

    with trt.OnnxParser(network, TRT_LOGGER) as parser:
        # Builder config
        builder_config.max_workspace_size =  2 ** 30 * 3 #3 GiB
        builder_config.set_flag(trt.BuilderFlag.FP16)

        with open(filepath, "rb") as file:
            if not parser.parse(file.read()):
                raise Exception(f"Failed to parse ONNX file: {filepath}")

        # Mark model outputs
        if not network.num_outputs:
            last_layer = network.get_layer(network.num_layers - 1)
            if not last_layer.num_outputs:
                raise Exception(f"ONNX file {filepath} contains no outputs")
            for i in range(last_layer.num_outputs):
                network.mark_output(last_layer.get_output(i))

        # Create optimization profile for each input and batch size
        inputs = [network.get_input(i) for i in range(network.num_inputs)]
        if all([input.shape[0] > -1 for input in inputs]):
            profiles = []
            for input in inputs:
                profile = builder.create_optimization_profile()
                bs, shape = input.shape[0], input.shape[1:]
                profile.set_shape(input.name, min=(bs, *shape), opt=(bs, *shape), max=(bs, *shape))
                profiles.append(profile)
        else:
            profiles = {}
            batch_sizes = [1, 3]
            for bs in batch_sizes:
                profiles[bs] = builder.create_optimization_profile()
                for input in inputs:
                    shape = input.shape[1:]
                    profiles[bs].set_shape(input.name, min=(bs, * shape), opt=(bs, *shape), max=(bs, *shape))
            profiles = list(profiles.values())
        # Add profiles to builder config
        for profile in profiles:
            builder_config.add_optimization_profile(profile)

        # Build engine
        engine = builder.build_engine(network, config=builder_config)
        return engine

def save_engine(engine, filepath):
    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    file = open(filepath, "wb")
    file.write(engine.serialize())

def load_engine(filepath):
    if not os.path.exists(filepath):
        os.makedirs("onnx", exist_ok=True)
        filename = os.path.splitext(os.path.basename(filepath))[0]
        onnx_path = os.path.join("onnx", f"{filename}.onnx")
        # Download ONNX file from AWS
        if not os.path.exists(onnx_path):
            aws.download_file("cz-models", f"onnx/{os.path.basename(onnx_path)}", onnx_path)
            #aws.download_file("cz-dataset", "onnx/old/resnet18_iou0.2_fpn_person_864x486_19-10-2020.onnx", onnx_path)
        # Load engine from ONNX file
        engine = load_onnx(onnx_path)
        # Save engine as TRT
        save_engine(engine, os.path.abspath(filepath))
    else:
        engine = load_trt(filepath)
    return engine
