import cv2
from config import config

def expand_bbox(frame, bbox, ratio=0.08):
    frame_h, frame_w = frame.shape[:2]
    x1, y1, x2, y2 = bbox
    box_w = x2 - x1
    box_h = y2 - y1
    # Expand bbox to capture larger image
    x1 = int(max(0, x1 - ratio * box_w))
    x2 = int(min(frame_w, x2 + ratio * box_w))
    y1 = int(max(0, y1 - ratio * box_h))
    y2 = int(min(frame_h, y2 + ratio * box_h))
    return frame[y1:y2, x1:x2]

def filter_bbox(frame, bbox):
    frame_h, frame_w = frame.shape[:2]
    x1, y1, x2, y2 = bbox
    # Filter bbox by width
    width = (x2 - x1) / frame_w
    if width < config.logic.min_bbox_width or width > config.logic.max_bbox_width:
        return False
    # Filter bbox by height
    height = (y2 - y1) / frame_h
    if height < config.logic.min_bbox_height or height > config.logic.max_bbox_height:
        return False
    # Filter bbox by aspect ratio
    ratio = height / width
    if ratio < config.logic.min_bbox_ratio or ratio > config.logic.max_bbox_ratio:
        return False
    return True
