from .task_queue import TaskQueue
from .task_scheduler import TaskScheduler

queue = TaskQueue("default", n_workers=2)
video_queue = TaskQueue("video", n_workers=2)
alarm_queue = TaskQueue("alarm", n_workers=1)
