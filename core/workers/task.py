class Task:

    def __init__(self,
        func, args=(), kwargs={},
        name="task",
        max_retries=-1,
        exception_func=None,
        exception_args=()
    ):
        self.name = name
        self.func = func
        self.args = args
        self.kwargs = kwargs

        self.retries = 0
        self.max_retries = max_retries
        self.exception_func = exception_func
        self.exception_args = exception_args
