import datetime
import threading
import time
from pytz import timezone

hk_timezone = timezone("Asia/Hong_Kong")

class TaskScheduler:

    def __init__(self,
        queue,
        func, args=(),
        name="scheduler",
        exception_func=None, exception_args=(),
        interval=60, start_hour=0, end_hour=23
    ):
        self.name = name
        self.queue = queue
        self.func = func
        self.args = args
        self.exception_func = exception_func
        self.exception_args = exception_args
        self.interval = interval
        self.start_time = datetime.time(hour=start_hour, tzinfo=hk_timezone)
        self.end_time = datetime.time(hour=end_hour, minute=59, tzinfo=hk_timezone)
        self.stopped = False

    def start(self):
        threading.Thread(target=self._schedule_task, daemon=True).start()
        return self

    def stop(self):
        self.stopped = True

    def set_interval(self, interval):
        self.interval = interval

    def _schedule_task(self):
        while True:
            if self.stopped: break

            current_time = datetime.datetime.now().astimezone(hk_timezone).time()
            if self.start_time <= current_time <= self.end_time:
                self.queue.enqueue(
                    self.func, self.args,
                    name=self.name,
                    max_retries=0,
                    exception_func=self.exception_func,
                    exception_args=self.exception_args
                )

                time.sleep(self.interval)
