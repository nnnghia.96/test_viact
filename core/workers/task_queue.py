import time
from .task import Task
from config import config
from core.logging import logger
from queue import Queue
from threading import Thread

class TaskQueue:

    def __init__(self, name, n_workers=10):
        self.name = name
        self.queue = Queue(maxsize=0)
        self.n_workers = n_workers
        self.stopped = False

    def start(self):
        for i in range(self.n_workers):
            Thread(target=self._handle_request, daemon=True).start()

    def enqueue(self,
        func, args=(), kwargs={},
        name="task",
        max_retries=-1,
        exception_func=None,
        exception_args=()
    ):
        task = Task(
            func, args, kwargs,
            name=name,
            max_retries=max_retries,
            exception_func=exception_func,
            exception_args=exception_args
        )
        self.queue.put(task)

    def close(self):
        logger.info(f"Closing {self.name} Queue ...")
        self.stopped = True
        self.queue.join()

    def _handle_request(self):
        while True:
            task = self.queue.get()
            if not self.stopped:
                try:
                    task.func(*task.args, **task.kwargs)
                except Exception as exception:
                    logger.info(f"Queue [{self.name}] Task [{task.name}] failed: {exception}")
                    if task.max_retries == -1 or task.max_retries > 0:

                        if task.max_retries != -1 and task.retries >= task.max_retries:
                            logger.info(f"Queue [{self.name}] Task [{task.name}] failed: exceeded max retries")
                            if task.exception_func is not None:
                                task.exception_func(*task.exception_args)
                        else:
                            task.retries += 1
                            logger.info(f"Queue [{self.name}] Task [{task.name}] failed: retries {task.retries}")
                            self.queue.put(task)
                    else:
                        assert task.max_retries == 0
                        if task.exception_func is not None:
                            task.exception_func(*task.exception_args)

            self.queue.task_done()
