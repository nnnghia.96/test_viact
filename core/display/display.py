import cv2
import numpy as np
from .colors import *
from config import config
from imutils.video import FPS
from math import ceil
from math import sqrt

class Display:

    def __init__(self, n_monitors):

        self.height = config.display.height
        self.width = config.display.width
        self.image = np.zeros((self.height, self.width, 3), dtype=np.uint8)

        # Display all monitor frames in a grid
        self.n_monitors = n_monitors
        self.n_cols = ceil(sqrt(n_monitors))
        self.n_rows = ceil(n_monitors / self.n_cols)
        self.cell_w = self.width // self.n_cols
        self.cell_h = self.height // self.n_rows

        self.fps = FPS().start()

    def show(self, frames):
        assert len(frames) == self.n_monitors
        cell_w, cell_h = self.cell_w, self.cell_h

        # Set background to gray
        self.image.fill(100)
        # Draw each monitor frame
        for i, frame in enumerate(frames):
            if frame is None: continue
            # Find position of frame in grid
            row = i // self.n_cols
            col = i % self.n_cols
            frame = cv2.resize(frame, (cell_w, cell_h))
            self.image[row*cell_h : (row+1)*cell_h, col*cell_w : (col+1)*cell_w] = frame

        self.fps.update()
        self.fps.stop()
        cv2.putText(self.image, f"FPS: {self.fps.fps():.2f}", (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, BLACK, 2)

        cv2.imshow("Video", self.image)
