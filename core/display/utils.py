import cv2
import imutils
import numpy as np
from .colors import *
from config import config

def draw_detections(frame, detections):
    for detection in detections:
        x1, y1, x2, y2 = detection.bbox
        if len(detection.trigger_events) > 0:
        #if False:
            # Draw bounding box
            cv2.rectangle(frame, (x1, y1), (x2, y2), RED, 2)

            # labels
            labels = [event for event in detection.trigger_events]

            if config.display.display_trigger_frames:
                labels = [f"{event}: {frames}" for event, frames in detection.n_trigger_frames.items()]

            if config.display.display_trigger_alerts:
                labels.append(f"alerts: {detection.n_trigger_alerts}")

            # Draw label background
            background_width = 0
            background_height = 0
            for label in labels:
                (label_width, label_height), baseline = cv2.getTextSize(label, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)
                background_width = max(background_width, label_width)
                background_height += label_height + 2
            cv2.rectangle(frame, (x1, y1 - background_height), (x1 + background_width, y1), RED, -1)
            # Draw labels
            for i, label in enumerate(labels):
                position = (x1, y1 - i * (label_height + 2))
                cv2.putText(frame, label, position, cv2.FONT_HERSHEY_SIMPLEX, 0.5, WHITE, 1)
        else:
            # Draw bounding box
            cv2.rectangle(frame, (x1, y1), (x2, y2), WHITE, 2)
    return frame

def draw_tracked_objects(frame, tracked_objects):
    for id, tracked_object in tracked_objects.items():
        # Draw centroid
        cx, cy = tracked_object.centroid
        cv2.circle(frame, (cx, cy), 4, WHITE, -1)
        # Draw label
        label = f"ID {id}"
        (label_width, _), _ = cv2.getTextSize(label, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)
        cv2.putText(frame, label, (cx - label_width // 2, cy - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, WHITE, 1)
        # Show history of centroids trail
        if config.display.display_centroids_trail:
            for i in range(1, len(tracked_object.centroids)):
                thickness = int((float(i) / tracked_object.centroids.maxlen + 1) * 5)
                cv2.line(frame, tracked_object.centroids[i - 1], tracked_object.centroids[i], WHITE, thickness)
    return frame

def draw_zones(frame, zones):
    if frame is not None:
        frame_h, frame_w = frame.shape[:2]
        output = frame.copy()
        for zone in zones:
            input_h, input_w = zone.input_h, zone.input_w
            overlay = cv2.resize(frame, (input_w, input_h))
            if zone.type == "circle":
                cv2.circle(overlay, (zone.cx, zone.cy), zone.r, zone.color, -1)
            if zone.type == "square":
                cv2.rectangle(overlay, (zone.x1, zone.y1), (zone.x2, zone.y2), zone.color, -1)
            if zone.type == "polygon":
                cv2.drawContours(overlay, [zone.coords], 0, zone.color, -1)
            overlay = cv2.resize(overlay, (frame_w, frame_h))
            output = cv2.addWeighted(overlay, 0.2, output, 0.8, 0)
        return output

def draw_detected_face(frame, bbox, label, progress=0):
    # Find circle center and radius from bbox
    x1, y1, x2, y2 = bbox
    cx = (x1 + x2) // 2
    cy = (y1 + y2) // 2
    r = max((x2 - x1) // 2, (y2 - y1) // 2)
    if label is None:
        # Draw red circle for unmatched face
        cv2.circle(frame, (cx, cy), r, RED, 2)
    else:
        # Draw green circle with progress for matched face
        cv2.putText(frame, label, (x1, y1 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, WHITE, 1)
        cv2.ellipse(frame, (cx, cy), (r, r), 0, 0, 360 * progress, GREEN, 2)
    return frame
